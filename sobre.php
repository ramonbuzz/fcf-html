<?php include('partials/header.php'); ?>


<main>

    <section>

        <h5 class="col-100 center">Sobre</h5>

        <h1 class="col-100 center">Faculdade Católica de Fortaleza</h1>

        <div class="col-100 texto">

            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quos, laboriosam modi molestiae, nesciunt alias
                eius nostrum reiciendis nam quae, magni quibusdam nemo! Hic cumque reprehenderit iusto nulla atque fuga
                eaque totam repellendus magnam odit consequatur voluptate, ad et, quisquam earum dolore corporis.
                Incidunt perferendis est itaque nemo similique optio tempore!</p>

            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, quibusdam. Accusantium cupiditate
                explicabo ut tenetur expedita neque mollitia officia doloremque autem, veritatis ullam exercitationem,
                quo reprehenderit asperiores necessitatibus enim est distinctio id aliquid harum! Nulla vel deserunt
                officia vitae dolor dolore laudantium eos ad, quas consequatur. Non dolor labore cum illum vitae vel nam
                quas, odit pariatur illo voluptates suscipit.</p>

        </div>

        <article class="col-100">

            <div class="sobre-area">

                <div class="sobre-cartao">
                    <h4>Missão</h4>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates corporis commodi, qui vero
                        eum architecto suscipit iusto sit a debitis.</p>
                </div>

                <div class="sobre-cartao">
                    <h4>Visão</h4>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates corporis commodi, qui vero
                        eum architecto suscipit iusto sit a debitis.</p>
                </div>

                <div class="sobre-cartao">
                    <h4>Valores</h4>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates corporis commodi, qui vero
                        eum architecto suscipit iusto sit a debitis.</p>
                </div>

            </div>

            <div class="bg-cartao">
                <img src="assets/img/fotos/06.jpg" alt="">
            </div>

        </article>

        <h5 class="col-100 center">História</h5>

        <h1 class="col-100 center">Timeline com datas importantes</h1>

        <article class="col-100 timeline owl-carousel owl-theme">

        <div>

            <div class="timeline-bloco">

                <div class="timeline-area">

                    <h1>1850 - 1900</h1>

                    <div class="timeline-cartao">
                        <h4>Um início difícil, mas bonito</h4>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Tempora, aut pariatur iste
                            alias unde magnam doloremque officia dolores mollitia qui itaque ex beatae quas corporis
                            culpa cum! Quidem dolor aperiam, totam illum recusandae autem! Sunt beatae aperiam
                            doloribus, qui quisquam aut eum impedit aliquid nulla voluptas sequi harum rem
                            perferendis optio totam inventore maiores quia explicabo numquam amet. Nesciunt, soluta
                            distinctio optio vero beatae accusamus reiciendis ullam perferendis commodi eius
                            voluptate. Temporibus, sint a in, quasi debitis numquam accusamus quas cumque qui
                            voluptatum dolor impedit. Id nulla nemo voluptatem recusandae, quidem eum natus
                            inventore, laboriosam facere molestiae, vitae consequatur laudantium.</p>
                    </div>

                </div>

                <div class="bg-cartao">
                    <img src="assets/img/fotos/03.jpg" alt="">
                </div>

            </div>

        </div>

        <div>

            <div class="timeline-bloco">

                <div class="timeline-area">

                    <h1>1850 - 1900</h1>

                    <div class="timeline-cartao">
                        <h4>Um início difícil, mas bonito</h4>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Tempora, aut pariatur iste
                            alias unde magnam doloremque officia dolores mollitia qui itaque ex beatae quas corporis
                            culpa cum! Quidem dolor aperiam, totam illum recusandae autem! Sunt beatae aperiam
                            doloribus, qui quisquam aut eum impedit aliquid nulla voluptas sequi harum rem
                            perferendis optio totam inventore maiores quia explicabo numquam amet. Nesciunt, soluta
                            distinctio optio vero beatae accusamus reiciendis ullam perferendis commodi eius
                            voluptate. Temporibus, sint a in, quasi debitis numquam accusamus quas cumque qui
                            voluptatum dolor impedit. Id nulla nemo voluptatem recusandae, quidem eum natus
                            inventore, laboriosam facere molestiae, vitae consequatur laudantium.</p>
                    </div>

                </div>

                <div class="bg-cartao">
                    <img src="assets/img/fotos/03.jpg" alt="">
                </div>

            </div>

        </div>

        </article>

    </section>

</main>

<?php include('partials/footer.php'); ?>