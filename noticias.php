<?php include('partials/header.php'); ?>
    
    
    <main>

    <section>

        <h5 class="col-100 center">Notícias</h5>

        <article class="col-50">

            <div class="noticia-cartao">
                <figure><img src="assets/img/fotos/01.jpg" alt="Foto da notícia"></figure>
                <a href="noticias-interna.php">
                    <h2>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur?</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In quaerat inventore veniam accusamus, dolore neque ad quam voluptatibus placeat asperiores soluta cum, omnis corrupti rem ipsam iusto cumque deserunt quibusdam.</p>
                    <small>15 NOV 2019</small>
                    <small>Leia mais</small>
                </a>
            </div>

        </article>

        <article class="col-50">

            <div class="noticia-cartao">
                <figure><img src="assets/img/fotos/02.jpg" alt="Foto da notícia"></figure>
                <a href="noticias-interna.php">
                    <h2>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur?</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In quaerat inventore veniam accusamus, dolore neque ad quam voluptatibus placeat asperiores soluta cum, omnis corrupti rem ipsam iusto cumque deserunt quibusdam.</p>
                    <small>15 NOV 2019</small>
                    <small>Leia mais</small>
                </a>
            </div>

        </article>

        <article class="col-33">

                <div class="noticia-cartao">
                    <figure><img src="assets/img/fotos/02.jpg" alt="Foto da notícia"></figure>
                    <a href="#">
                        <h2>Lorem ipsum dolor sit amet consectetur</h2>
                        <small>15 NOV 2019</small>
                        <small>Leia mais</small>
                    </a>
                </div>

            </article>

            <article class="col-33">

                <div class="noticia-cartao">
                    <figure><img src="assets/img/fotos/03.jpg" alt="Foto da notícia"></figure>
                    <a href="#">
                        <h2>Lorem ipsum dolor sit amet consectetur</h2>
                        <small>15 NOV 2019</small>
                        <small>Leia mais</small>
                    </a>
                </div>

            </article>

            <article class="col-33">

                <div class="noticia-cartao">
                    <figure><img src="assets/img/fotos/04.jpg" alt="Foto da notícia"></figure>
                    <a href="#">
                        <h2>Lorem ipsum dolor sit amet consectetur</h2>
                        <small>15 NOV 2019</small>
                        <small>Leia mais</small>
                    </a>
                </div>

            </article>

        <article class="col-33">

                <div class="noticia-cartao">
                    <figure><img src="assets/img/fotos/02.jpg" alt="Foto da notícia"></figure>
                    <a href="#">
                        <h2>Lorem ipsum dolor sit amet consectetur</h2>
                        <small>15 NOV 2019</small>
                        <small>Leia mais</small>
                    </a>
                </div>

            </article>

            <article class="col-33">

                <div class="noticia-cartao">
                    <figure><img src="assets/img/fotos/03.jpg" alt="Foto da notícia"></figure>
                    <a href="#">
                        <h2>Lorem ipsum dolor sit amet consectetur</h2>
                        <small>15 NOV 2019</small>
                        <small>Leia mais</small>
                    </a>
                </div>

            </article>

            <article class="col-33">

                <div class="noticia-cartao">
                    <figure><img src="assets/img/fotos/04.jpg" alt="Foto da notícia"></figure>
                    <a href="#">
                        <h2>Lorem ipsum dolor sit amet consectetur</h2>
                        <small>15 NOV 2019</small>
                        <small>Leia mais</small>
                    </a>
                </div>

            </article>

        <article class="col-33">

                <div class="noticia-cartao">
                    <figure><img src="assets/img/fotos/02.jpg" alt="Foto da notícia"></figure>
                    <a href="#">
                        <h2>Lorem ipsum dolor sit amet consectetur</h2>
                        <small>15 NOV 2019</small>
                        <small>Leia mais</small>
                    </a>
                </div>

            </article>

            <article class="col-33">

                <div class="noticia-cartao">
                    <figure><img src="assets/img/fotos/03.jpg" alt="Foto da notícia"></figure>
                    <a href="#">
                        <h2>Lorem ipsum dolor sit amet consectetur</h2>
                        <small>15 NOV 2019</small>
                        <small>Leia mais</small>
                    </a>
                </div>

            </article>

            <article class="col-33">

                <div class="noticia-cartao">
                    <figure><img src="assets/img/fotos/04.jpg" alt="Foto da notícia"></figure>
                    <a href="#">
                        <h2>Lorem ipsum dolor sit amet consectetur</h2>
                        <small>15 NOV 2019</small>
                        <small>Leia mais</small>
                    </a>
                </div>

            </article>

        <article class="col-33">

                <div class="noticia-cartao">
                    <figure><img src="assets/img/fotos/02.jpg" alt="Foto da notícia"></figure>
                    <a href="#">
                        <h2>Lorem ipsum dolor sit amet consectetur</h2>
                        <small>15 NOV 2019</small>
                        <small>Leia mais</small>
                    </a>
                </div>

            </article>

            <article class="col-33">

                <div class="noticia-cartao">
                    <figure><img src="assets/img/fotos/03.jpg" alt="Foto da notícia"></figure>
                    <a href="#">
                        <h2>Lorem ipsum dolor sit amet consectetur</h2>
                        <small>15 NOV 2019</small>
                        <small>Leia mais</small>
                    </a>
                </div>

            </article>

            <article class="col-33">

                <div class="noticia-cartao">
                    <figure><img src="assets/img/fotos/04.jpg" alt="Foto da notícia"></figure>
                    <a href="#">
                        <h2>Lorem ipsum dolor sit amet consectetur</h2>
                        <small>15 NOV 2019</small>
                        <small>Leia mais</small>
                    </a>
                </div>

            </article>

    </section>

    </main>

    <?php include('partials/footer.php'); ?>