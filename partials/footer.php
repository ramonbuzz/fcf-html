<div class="footer-1">

<section>

    <article class="col-25">

        <ul>
            <li>Acadêmico</li>
            <li><a href="#" target="_blank">Avaliação institucional</a></li>
            <li><a href="#" target="_blank">Solicitar certificado de eventos</a></li>
            <li><a href="#" target="_blank">Calendário acadêmico</a></li>
        </ul>

    </article>

    <article class="col-25">

        <ul>
            <li>Documentos</li>
            <li><a href="#" target="_blank">Bolsas e financiamentos</a></li>
            <li><a href="#" target="_blank">Manual institucional</a></li>
            <li><a href="#" target="_blank">Regulamentos institucionais</a></li>
        </ul>

    </article>

    <article class="col-25">

        <ul>
            <li>Publicações</li>
            <li><a href="#" target="_blank">Revista científica IES</a></li>
            <li><a href="#" target="_blank">Revista científica dos cursos</a></li>
            <li><a href="#" target="_blank">Acervo científico</a></li>
        </ul>

    </article>

    <article class="col-25">

        <ul>
            <li>Links úteis</li>
            <li><a href="#" target="_blank">Núcleo de Apoio Psicopedagógico</a></li>
            <li><a href="#" target="_blank">Comitê de Ética em Pesquisa com seres humanos</a></li>
            
        </ul>

    </article>

</section>

</div>

<div class="footer-2">

<section>

    <article class="col-25">

        <img src="assets/img/logo-2.png" alt="Logotipo antigo">

    </article>

    <article class="col-50">

        <p>Rua Tenente Benévolo, 201 - Centro | Fortaleza, Ceará - Brasil</p>
        <a href="mailto:catolicafort@catolicadefortaleza.edu.br">catolicafort@catolicadefortaleza.edu.br</a>
        <p>(85) 3453.2150 / (85) 99690.0302</p>

    </article>

    <article class="col-25">

        <ul>
            <li>Contato
                <ul class="social">
                    <li><a href="#"><img src="assets/img/icone-wpp.png" alt=""></a> <a href="#"><img src="assets/img/icone-face.png" alt=""></a></li>
                </ul>
            </li>
            <li><a href="modal/trabalhe.php" rel="modal:open">Trabalhe conosco</a></li>
            <li><a href="modal/contato.php" rel="modal:open">Fale conosco</a></li>
        </ul>

    </article>

</section>

</div>

<footer>

2019 - Faculdade Católica de Fortaleza. Todos os direitos reservados

</footer>

<div class="chat">
<a href="#"><img src="assets/img/chat.png" alt="Chat"></a>
</div>

<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/modal.js"></script>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/js/funcoes.js"></script>

</body>

</html>