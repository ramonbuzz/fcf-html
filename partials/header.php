<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/css/modal.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="assets/css/estilo.css">
    <title>Faculdade Católica de Fortaleza</title>
</head>
<body>

    <header>
        
        <nav>

            <a href="index.php" class="col-30"><img src="assets/img/logo.svg" alt="Página Incial"></a>

            <label for="hamburger" class="col-30 right">&#9776;</label>
            <input type="checkbox" id="hamburger"/>
    
            <ul class="col-70">
                <li><a href="cursos.php">Cursos</a></li>
                <li><a href="sobre.php">Sobre</a></li>
                <li><a href="noticias.php">Notícias</a></li>
                <li><a href="eventos.php">Eventos</a></li>
                <li><a href="#">Biblioteca</a></li>
                <li><a href="modal/inscrevase.php" rel="modal:open" class="btn-sec">Inscreva-se</a></li>
                <li><a href="#" class="btn-pri" target="_blank">Aluno online</a></li>
            </ul>

        </nav>

    </header>