<?php include('partials/header.php'); ?>
    
    
    <main>

    <section>

        <h5 class="col-100 center">Cursos</h5>

        <article class="col-100">

            <div class="cursos-area">

                <img src="assets/img/icone-graduacao.png" alt="">
                
                <div class="cursos-cartao">
                    <h4>Graduação</h4>

                    <div class="cursos-accordion">
                        <button class="accordion">Ciências humanas</button>
                        <div class="panel">
                            <p><a href="cursos-interna.php">Bacharelado em Filosofia</a></p>
                            <p><a href="cursos-interna.php">Bacharelado em Sociologia</a></p>
                            <p><a href="cursos-interna.php">Bacharelado em Antropologia</a></p>
                        </div>
                    </div>

                    <div class="cursos-accordion">
                        <button class="accordion">Ciências exatas</button>
                        <div class="panel">
                            <p><a href="cursos-interna.php">Bacharelado em Filosofia</a></p>
                            <p><a href="cursos-interna.php">Bacharelado em Sociologia</a></p>
                            <p><a href="cursos-interna.php">Bacharelado em Antropologia</a></p>
                        </div>
                    </div>

                    <div class="cursos-accordion">
                        <button class="accordion">Ciências naturais</button>
                        <div class="panel">
                            <p><a href="cursos-interna.php">Bacharelado em Filosofia</a></p>
                            <p><a href="cursos-interna.php">Bacharelado em Sociologia</a></p>
                            <p><a href="cursos-interna.php">Bacharelado em Antropologia</a></p>
                        </div>
                    </div>

                </div>

            </div>

            <div class="bg-cartao">
                <img src="assets/img/fotos/01.jpg" alt="">
            </div>

        </article>

        <article class="col-100">

            <div class="cursos-area">

                <img src="assets/img/icone-mestrado.png" alt="">
                
                <div class="cursos-cartao">
                    <h4>Mestrado</h4>

                    <div class="cursos-accordion">
                        <button class="accordion">Ciências humanas</button>
                        <div class="panel">
                            <p><a href="cursos-interna.php">Bacharelado em Filosofia</a></p>
                            <p><a href="cursos-interna.php">Bacharelado em Sociologia</a></p>
                            <p><a href="cursos-interna.php">Bacharelado em Antropologia</a></p>
                        </div>
                    </div>

                    <div class="cursos-accordion">
                        <button class="accordion">Ciências exatas</button>
                        <div class="panel">
                            <p><a href="cursos-interna.php">Bacharelado em Filosofia</a></p>
                            <p><a href="cursos-interna.php">Bacharelado em Sociologia</a></p>
                            <p><a href="cursos-interna.php">Bacharelado em Antropologia</a></p>
                        </div>
                    </div>

                    <div class="cursos-accordion">
                        <button class="accordion">Ciências naturais</button>
                        <div class="panel">
                            <p><a href="cursos-interna.php">Bacharelado em Filosofia</a></p>
                            <p><a href="cursos-interna.php">Bacharelado em Sociologia</a></p>
                            <p><a href="cursos-interna.php">Bacharelado em Antropologia</a></p>
                        </div>
                    </div>

                </div>

            </div>

            <div class="bg-cartao">
                <img src="assets/img/fotos/02.jpg" alt="">
            </div>

        </article>

        <article class="col-100">

            <div class="cursos-area">

                <img src="assets/img/icone-especializacao.png" alt="">
                
                <div class="cursos-cartao">
                    <h4>Especialização</h4>

                    <div class="cursos-accordion">
                        <button class="accordion">Ciências humanas</button>
                        <div class="panel">
                            <p><a href="cursos-interna.php">Bacharelado em Filosofia</a></p>
                            <p><a href="cursos-interna.php">Bacharelado em Sociologia</a></p>
                            <p><a href="cursos-interna.php">Bacharelado em Antropologia</a></p>
                        </div>
                    </div>

                    <div class="cursos-accordion">
                        <button class="accordion">Ciências exatas</button>
                        <div class="panel">
                            <p><a href="cursos-interna.php">Bacharelado em Filosofia</a></p>
                            <p><a href="cursos-interna.php">Bacharelado em Sociologia</a></p>
                            <p><a href="cursos-interna.php">Bacharelado em Antropologia</a></p>
                        </div>
                    </div>

                    <div class="cursos-accordion">
                        <button class="accordion">Ciências naturais</button>
                        <div class="panel">
                            <p><a href="cursos-interna.php">Bacharelado em Filosofia</a></p>
                            <p><a href="cursos-interna.php">Bacharelado em Sociologia</a></p>
                            <p><a href="cursos-interna.php">Bacharelado em Antropologia</a></p>
                        </div>
                    </div>

                </div>

            </div>

            <div class="bg-cartao">
                <img src="assets/img/fotos/03.jpg" alt="">
            </div>

        </article>

        <article class="col-100">

            <div class="cursos-area">

                <img src="assets/img/icone-extensao.png" alt="">
                
                <div class="cursos-cartao">
                    <h4>Extensão</h4>

                    <div class="cursos-accordion">
                        <button class="accordion">Ciências humanas</button>
                        <div class="panel">
                            <p><a href="cursos-interna.php">Bacharelado em Filosofia</a></p>
                            <p><a href="cursos-interna.php">Bacharelado em Sociologia</a></p>
                            <p><a href="cursos-interna.php">Bacharelado em Antropologia</a></p>
                        </div>
                    </div>

                    <div class="cursos-accordion">
                        <button class="accordion">Ciências exatas</button>
                        <div class="panel">
                            <p><a href="cursos-interna.php">Bacharelado em Filosofia</a></p>
                            <p><a href="cursos-interna.php">Bacharelado em Sociologia</a></p>
                            <p><a href="cursos-interna.php">Bacharelado em Antropologia</a></p>
                        </div>
                    </div>

                    <div class="cursos-accordion">
                        <button class="accordion">Ciências naturais</button>
                        <div class="panel">
                            <p><a href="cursos-interna.php">Bacharelado em Filosofia</a></p>
                            <p><a href="cursos-interna.php">Bacharelado em Sociologia</a></p>
                            <p><a href="cursos-interna.php">Bacharelado em Antropologia</a></p>
                        </div>
                    </div>

                </div>

            </div>

            <div class="bg-cartao">
                <img src="assets/img/fotos/04.jpg" alt="">
            </div>

        </article>

    </section>

    </main>

    <?php include('partials/footer.php'); ?>