<?php include('partials/header.php'); ?>
    
    <div class="banner-home owl-carousel owl-theme">
        <div><img src="assets/img/banner.png" alt=""></div>
        <div><img src="assets/img/banner.png" alt=""></div>
        <div><img src="assets/img/banner.png" alt=""></div>
    </div>

    <main>

        <section>

            <h1 class="col-100 center">Últimas notícias</h1>

            <article class="col-50">

                <div class="noticia-cartao">
                    <figure><img src="assets/img/fotos/01.jpg" alt="Foto da notícia"></figure>
                    <a href="noticias-interna.php">
                        <h2>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur?</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In quaerat inventore veniam accusamus, dolore neque ad quam voluptatibus placeat asperiores soluta cum, omnis corrupti rem ipsam iusto cumque deserunt quibusdam.</p>
                        <small>15 NOV 2019</small>
                        <small>Leia mais</small>
                    </a>
                </div>

            </article>

            <article class="col-50">

                <div class="noticia-cartao">
                    <figure><img src="assets/img/fotos/02.jpg" alt="Foto da notícia"></figure>
                    <a href="noticias-interna.php">
                        <h2>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur?</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In quaerat inventore veniam accusamus, dolore neque ad quam voluptatibus placeat asperiores soluta cum, omnis corrupti rem ipsam iusto cumque deserunt quibusdam.</p>
                        <small>15 NOV 2019</small>
                        <small>Leia mais</small>
                    </a>
                </div>

            </article>

        </section>

        <section>

            <h1 class="col-100 center">Eventos acadêmicos</h1 class="col-100">

            <article class="col-33">

                <div class="noticia-cartao">
                    <figure><img src="assets/img/fotos/02.jpg" alt="Foto da notícia"></figure>
                    <a href="#">
                        <h2>Lorem ipsum dolor sit amet consectetur</h2>
                        <small>15 NOV 2019</small>
                        <small>Leia mais</small>
                    </a>
                </div>

            </article>

            <article class="col-33">

                <div class="noticia-cartao">
                    <figure><img src="assets/img/fotos/03.jpg" alt="Foto da notícia"></figure>
                    <a href="#">
                        <h2>Lorem ipsum dolor sit amet consectetur</h2>
                        <small>15 NOV 2019</small>
                        <small>Leia mais</small>
                    </a>
                </div>

            </article>

            <article class="col-33">

                <div class="noticia-cartao">
                    <figure><img src="assets/img/fotos/04.jpg" alt="Foto da notícia"></figure>
                    <a href="#">
                        <h2>Lorem ipsum dolor sit amet consectetur</h2>
                        <small>15 NOV 2019</small>
                        <small>Leia mais</small>
                    </a>
                </div>

            </article>

        </section>

        <section class="bg-full">

            
            <div class="conheca-cartao center">
                <h3>Você sabia que somos a primeira faculdade de fortaleza?</h3>
                <a href="sobre.php" class="btn-ter">Conheça nossa história</a>
            </div>

        </section>

        <section>

            <h1 class="col-100 center">Temos cursos nas áreas de</h1 class="col-100">

            <article class="col-50">

                <div class="areas-cartao">
                    <a href="#">
                        <img src="assets/img/icone-graduacao.png" alt="icone">
                        <h4>Graduação</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates corporis commodi, qui vero eum architecto suscipit iusto sit a debitis.</p>
                    </a>
                </div>

                <div class="bg-cartao">
                    <img src="assets/img/fotos/01.jpg" alt="">
                </div>

            </article>

            <article class="col-50">

                <div class="areas-cartao">
                    <a href="#">
                        <img src="assets/img/icone-mestrado.png" alt="icone">
                        <h4>Mestrado</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates corporis commodi, qui vero eum architecto suscipit iusto sit a debitis.</p>
                    </a>
                </div>

                <div class="bg-cartao">
                    <img src="assets/img/fotos/02.jpg" alt="">
                </div>

            </article>

            <article class="col-50">

                <div class="areas-cartao">
                    <a href="#">
                        <img src="assets/img/icone-especializacao.png" alt="icone">
                        <h4>Especialização</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates corporis commodi, qui vero eum architecto suscipit iusto sit a debitis.</p>
                    </a>
                </div>

                <div class="bg-cartao">
                    <img src="assets/img/fotos/03.jpg" alt="">
                </div>

            </article>

            <article class="col-50">

                <div class="areas-cartao">
                    <a href="#">
                        <img src="assets/img/icone-extensao.png" alt="icone">
                        <h4>Extensão</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates corporis commodi, qui vero eum architecto suscipit iusto sit a debitis.</p>
                    </a>
                </div>

                <div class="bg-cartao">
                    <img src="assets/img/fotos/04.jpg" alt="">
                </div>

            </article>

        </section>

    </main>

    <?php include('partials/footer.php'); ?>